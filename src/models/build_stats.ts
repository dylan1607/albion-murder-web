export interface BuildStats {
    builds: BuildStat[];
}

export interface BuildStat {
    build: Build;
    usages: number;
    average_item_power: number;
    kill_fame: number;
    death_fame: number;
    kills: number;
    deaths: number;
    assists: number;
    fame_ratio: number | null;
    win_rate: number;
}

export interface Build {
    main_hand: Item;
    off_hand: Item;
    head: Item;
    body: Item;
    shoe: Item;
    cape: Item;
}

export interface Item {
    type: string;
    en_name: string;
}

export function buildId(build: Build): string {
    return [
        build.body.type,
        build.main_hand.type,
        build.cape.type,
        build.shoe.type,
        build.off_hand.type,
        build.head.type,
    ].join();
}
