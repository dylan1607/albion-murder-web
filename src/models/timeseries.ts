export interface Datum {
    time: string;
    value: number;
}
